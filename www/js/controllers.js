angular.module('starter.controllers', [])

.controller('AgendaController', function ($scope) {
  $scope.navTitle = 'iClubs'
})

.controller('FotosController', function ($scope, Locales, $ionicFilterBar) {
  $scope.locales = Locales.all()
  $scope.remove = function (local) {
    Locales.remove(local)
  }

  $scope.favorito = function (local) {

  }

  $scope.showFilterBar = function () {
    filterBarInstance = $ionicFilterBar.show({
      items: $scope.locales,
      update: function (filteredItems, filterText) {
        $scope.locales = filteredItems
        if (filterText) {
          console.log(filterText)
        }
      }
    })
  }
})

.controller('AlbunesController', function ($scope, $stateParams, Locales) {
  var local_id = $stateParams.fotosId
  $scope.local = Locales.get($stateParams.fotosId)

  $scope.items = [
    {
      src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg',
      sub: ''
    },
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'},
            {src: 'http://www.ccta.ufpb.br/intrum/contents/categorias/cordofones/udecra/sem-imagem.jpg/@@images/3cb1ca55-87cc-45a0-8d02-52a15fabc728.jpeg'}
  ]
})

.controller('FavoritosController', function ($scope) {})

.controller('AjustesController', function ($scope) {
  $scope.settings = {
    enviarNotificaciones: true
  }
})
